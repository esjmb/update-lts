{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


module Lib
    ( someFunc
    ) where

import           Control.Monad                (join, unless)
import qualified Data.ByteString.Lazy.Char8   as L8
import           Data.Maybe                   (fromMaybe)
import           Data.Semigroup               ((<>))
import           Network.HTTP.Simple          as H
import           Options.Applicative
import           System.Console.ANSI
import           System.Directory
import           System.Directory.ProjectRoot
import           System.Environment
import           System.FilePath
import qualified System.IO.Strict             as S
import           Text.Regex

someFunc :: IO ()
someFunc =   join $ execParser =<< opts

data BuildType = LTS | NIGHTLY

doHandleOptions :: BuildType -> Maybe String -> IO ()
doHandleOptions bt path = do

  ver <- (case bt of
           LTS     -> ltsUrl
           NIGHTLY -> nightlyUrl ) >>= H.httpLBS  >>= return . extractVersion . L8.unpack . getResponseBody

  case ver of
    Nothing -> putStrLn $ "Could not find " ++ (case bt of
                                                 LTS     -> " lts"
                                                 NIGHTLY -> "nightly") ++ " version."
    Just ver' -> do

      tag <- tagsUrl >>= H.httpLBS >>= return . matchRegex (mkRegexWithOpts ("\"" ++ ver' ++ "\"") True True) . L8.unpack . getResponseBody
      case tag of
        Nothing -> putStrLn $ "Could not find " ++ ver' ++ " tag for fpco/stack-build. Aborting update."
        Just _ -> do
          f <- file
          stackyml <- S.readFile f
          writeFile f $ subRegex mtc' stackyml $ "resolver: " ++  ver'
          putStrLn $ "Resolver set to " ++ ver' ++ "."

 where
  -- | pull out lts-x.xx or nightly-xxx-xx-xx
  extractVersion :: String -> Maybe String
  extractVersion s = case matchRegex mtc s of
                         Nothing    -> Nothing
                         Just (x:_) -> Just x

  file = case path of
               Nothing -> do
                 pr <- fromMaybe (error "No project root found") <$> getProjectRootCurrent
                 let stackyml = pr </> "stack.yaml"
                 stackYmlExists <- doesFileExist stackyml
                 unless stackYmlExists $ do
                   error $ "Can't find a stack.yaml file from this directory"
                 return stackyml
               Just x -> return x

  --  regexpression for finding resolver string from url
  mtc = (mkRegex "resolver: ((lts|nightly|-|[.0123456789])+)")

  -- as above but with start of line matchotic
  mtc' = (mkRegex "^resolver: ((lts|nightly|-|[.0123456789])+)")

  nightlyUrl = H.parseRequest "https://www.stackage.org/nightly"
  ltsUrl = H.parseRequest "https://www.stackage.org/lts"
  tagsUrl = H.parseRequest "https://hub.docker.com/r/fpco/stack-build/tags"

opts :: IO (ParserInfo (IO ()))
opts = do
  progName <- getProgName

  return $ info (   helper
                <*> subparser
                       (  command "lts"
                                  (withInfo ( doHandleOptions LTS
                                          <$> yamlOpt
                                            ) "update the passed yaml file to the latest lts build." )
                       <> command "nightly"
                                  (withInfo ( doHandleOptions NIGHTLY
                                          <$> yamlOpt
                                            ) "update the passed yaml file to the latest nightly build." )))
             (  fullDesc
             <> progDesc (progName ++ " updates the stack.yaml file passed to the latest lts or nightly build available" ++
                          " from lts.org." ++
                          " Try " ++ whiteCode ++ progName ++ " --help " ++ resetCode ++ " for more information. To " ++
                          " see the details of any command, " ++  "try " ++ whiteCode ++ progName ++ " COMMAND --help" ++
                          resetCode ++ ". The application supports bash completion. To enable, " ++
                          "ensure you have bash-completion installed and enabled (see your OS for details), the " ++
                          whiteCode ++ progName ++ resetCode ++
                          " application in your PATH, and place the following in your ~/.bash_profile : " ++ whiteCode ++
                          "source < (" ++ progName ++ " --bash-completion-script `which " ++ progName ++ "`)" ++
                          resetCode )
             <> header  (redCode ++ "update-lts" ++ resetCode))
  where
    -- | helper functions to change color in ansi terminal output (mor for the fun of it)
    redCode   = setSGRCode [SetConsoleIntensity BoldIntensity , SetColor Foreground Vivid Red]
    whiteCode = setSGRCode [SetConsoleIntensity BoldIntensity , SetColor Foreground Vivid White]
    blueCode  = setSGRCode [SetConsoleIntensity BoldIntensity , SetColor Foreground Vivid Blue]
    resetCode = setSGRCode [Reset]

    yamlOpt = optional (strOption ( long "yaml"
                             <> short 'y'
                             <> help "The yaml file to change."))
    -- helpers to simplify the creation of command line options
    withInfo :: Parser a -> String -> ParserInfo a
    withInfo opts desc = info (helper <*> opts) $ progDesc desc
