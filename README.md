update-lts

A simple program that retrieves the latest lts build id from https://www.stackage.org/ and
updates the passed yaml file (or the stack.yml in the current project directories if not passed).

Clone the repository, and install as follows.

``` bash
stack install
```

run as follows:

``` bash
update-lts [lts|nightly] [-y stack.yaml]
```



all suggestions and updates welcome,

thanks,
Stephen.